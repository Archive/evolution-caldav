2008-05-13  Leonardo Ferreira Fontenelle  <leonardof@gnome.org>

	* configure.in: Added "pt_BR" do ALL_LINGUAS.

2007-10-14  Yannig Marchegay  <yannig@marchegay.org>

	* configure.in: Added 'oc' to ALL_LINGUAS.

2007-09-22  Gil Forcada  <gforcada@svn.gnome.org>

	* configure.in: Added 'ca' to ALL_LINGUAS

2007-08-16  Ilkka Tuohela  <hile@iki.fi>

	* configure.in: Added 'fi' to ALL_LINGUAS

2007-08-09  Inaki Larranaga Murgoitio  <dooteo@zundan.com>

	* configure.in: Added 'eu' to ALL_LINGUAS

2007-04-29  David Lodge <dave@cirt.net>

	* configure.in: Added 'en_GB' to ALL_LINGUAS

2007-04-04  Pema Geyleg  <pema.geyleg>

	* configure.in: Added 'dz' to ALL_LINGUAS

=== evolution-caldav 2.3.99.3 ===

2005-12-01  Dave Camp  <dave@novell.com>

	* configure.in: Bumped version to 2.3.99.3.

2005-12-01  Dave Camp  <dave@novell.com>

	* eplugin/Makefile.am (liborg_gnome_evolution_caldav_la_LIBADD):
	added org-gnome-evolution-caldav.eplug to CLEANFILES.

2005-10-07  Dave Camp  <dave@novell.com>

	* calendar/e-cal-backend-caldav.c (XPATH_STATUS): <D:status> is a
	child of <D:propstat>
	(caldav_server_list_objects): Add a Depth: 1 header to the report.
	(pack_cobj): Include timezones.

2005-09-27  Dave Camp  <dave@novell.com>

	* calendar/e-cal-backend-caldav.c (initialize_backend): Use
	SSL if specified.
	* eplugin/caldav-source.c (ssl_changed, oge_caldav): Added a "Use
	SSL" checkbox.

2005-09-26  Dave Camp  <dave@novell.com>

	* calendar/e-cal-backend-caldav.c (extract_objects) 
	(process_object): Some small fixes to receive_objects().

2005-09-20  Christian Kellner  <gicmo@gnome.org>

	* calendar/e-cal-backend-caldav.c: (pack_cobj),
	(caldav_create_object), (caldav_modify_object), (extract_objects),
	(process_object), (caldav_receive_objects), (caldav_send_objects),
	(caldav_get_timezone):
	First take on receive_objects.

2005-09-10  Christian Kellner  <gicmo@gnome.org>

	* Makefile.am: Include eplugin.mk in EXTRA_DIST so it gets
	included in the tarball. This fixes bug #315875.
	Thanks to Dave Malcolm <dmalcolm@redhat.com> for pointing that out.

2005-08-30  Christian Kellner  <gicmo@gnome.org> 

	* configure.in: Add de to ALL_LINGUAS
	* eplugin.mk: Subsitute domain and localedir
	* eplugin/org-gnome-evolution-caldav.eplug.in:
	Set translation domain and localedir. 
	Also set load-on-startup so it we can assure the source group
	* po/POTFILES.in: Add caldav-source. as translatable.

2005-08-29  Christian Kellner  <gicmo@gnome.org>

	* eplugin/caldav-source.c: (oge_caldav):
	Prefill the username from the uri and hide username
	within the uri.

2005-08-29  Christian Kellner  <gicmo@gnome.org>

	* eplugin/Makefile.am:
	* eplugin/org-gnome-evolution-caldav.eplug:
	* eplugin/org-gnome-evolution-caldav.eplug.in:
	Removed the .eplug file and added the .eplug.in file
	so this picks up the evolution plugin dir automatically.
	Uuupps.

2005-08-28  Christian Kellner  <gicmo@gnome.org>

	* calendar/e-cal-backend-caldav.c: 
	(caldav_server_put_object): Update the etag member of the 
	CalDAV object with the value return from the PUT operation.

2005-08-28  Dave Camp  <dave@novell.com>

	* calendar/e-cal-backend-caldav.c
	(parse_report_response): check object->status rather than trying
	to reparse the status.

2005-08-25  Christian Kellner  <gicmo@gnome.org>

	* Import into its own module include eplugin

2005-08-25  Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c: 
	(synch_slave_loop), (caldav_is_loaded), 
	(e_cal_backend_caldav_dispose),
	(e_cal_backend_caldav_finalize):
	No need for locking foo in the dispose function.
	Let the slave free the lock.

2005-08-25  Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c: 
	(caldav_modify_object), (caldav_remove_object):
	Implementation of the function stubs. Patch
	provided by Jürg Billeter <j@bitron.ch>.
	This fixes bug #314494

2005-08-26  Jürg Billeter  <j@bitron.ch>

	reviewed by: Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c: (quote_etag),
	(xp_object_get_etag), (caldav_server_get_object),
	(caldav_server_put_object), (caldav_server_delete_object):
	
	Ensure to always use quoted ETags. Fixes #314493

2005-06-21  Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c: 
	(status_code_to_result): Also handle success status code;
	(caldav_server_put_object): Unref the SoupMessage.
	(caldav_server_delete_object): New function to delete
	objects on the CalDAV server.

2005-06-21  Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c:
	(e_cal_component_set_synch_state): New function to 
	indicate the synch state of on object (will be used
	to mark offline created/modified/deleted objects later)
	(e_cal_component_gen_href): New function to generate
	the uri where the object will be store. The implemenation
	totally sucks at the moment.
	(check_state): New helper function.
	(caldav_server_open_calendar): Fix setting of read only mode
	Also check for the DELETE header to decide about setting it.
	(caldav_server_put_object): New function to store objects on
	the CalDAV server.
	(initialize_backend): Fix the totally borken priv->uri.
	(caldav_create_object): Implement object creation!

2005-06-20  Christian Kellner  <gicmo@gnome.org>

	* backends/caldav/e-cal-backend-caldav.c: 
	(xp_object_get_etag):  New function to parse and 
	strip ETags from response xml.	
	(parse_report_response): Use xp_object_get_etag.
	(synchronize_object): More error checking and 
	fix notify_modifed argument.
	(initialize_backend): Always report changes for now.

2005-06-20  Christian Kellner  <gicmo@gnome.org>

	* backends/Makefile.am:
	Added CalDAV backend.
	
	* backends/caldav/Makefile.am:
	* backends/caldav/create-account.c: 
	Added small and dumb tool to create CalDAV sources. 
	
	* backends/caldav/e-cal-backend-caldav-factory.c:
	* backends/caldav/e-cal-backend-caldav-factory.h:
	* backends/caldav/e-cal-backend-caldav.c: 
	* backends/caldav/e-cal-backend-caldav.h:
	Added first version of the CalDAV backend.
